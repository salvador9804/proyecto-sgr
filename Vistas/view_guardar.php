<?php
session_start();

include_once '../Control/controlGuardar.php';

$objGuardar = new controlGuardar();

$posts = array('req_id', 'req_requerimiento', 'req_descripcion', 'req_fuente');

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="../css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="../js/materialize.min.js"></script>
	<title>Registro Requerimiento</title>
</head>
<body>
<?php   
  	if ($objGuardar->guardar($posts)) :
    	?>
	    <div class="card-panel teal lighten-5">
	        El requerimiento se registró exitosamente <a href="../index.php">botones</a>
	    </div>
    	<?php
    else :
    	?>
	    <div class="card-panel red lighten-5">
	        Hubo un error en el registro del requerimiento :/ <a href="../index.php">botones</a>
	    </div>
    	<?php
  	endif;
  ?>
</body>
</html>
