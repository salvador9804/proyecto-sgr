<?php

session_start();


include_once '../Control/controlReq.php';

$objReg = new controlReq();

$arrDatos = $objReg->edicion(/*$_SESSION['id_usuario']*/);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	
    <link rel="stylesheet" href="../css/materialize.min.css">

   
    <script src="../js/materialize.min.js"></script>
	
    <title>REQUERIMIENTOS</title>
	
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', () => {
			var elems = document.querySelectorAll('select');
			var instances = M.FormSelect.init(elems);
		});
	</script>
</head>
<body>
	<h2>Registro de Requerimientos</h2>

        <form id="formulario" action="view_guardar.php" method="post" enctype="multipart/form-data">
		
		<div class="input-field">
			<label>
				Requerimiento: 
			</label>
						<input type="text" id="requerimiento" name="req_requerimiento" value="<?php echo $arrDatos[1];?>"/>
		</div>
                
        <div class="input-field">
			<label>
				Descripción: 
			</label>
                        <input type="text" id="descripcion" name="req_descripcion" value="<?php echo $arrDatos[2];?>"/>
		</div>
                
        <div class="input-field">
			<label>
				Fuente: 
			</label>
                        <input type="text" id="fuente" name="req_fuente" value="<?php echo $arrDatos[3];?>"/>
		</div>

		<button type="button" class="btn" id="boton-guardar">Guardar</button>
		<input type="reset" name="reset" class="btn" value="Refrescar"/>

	</form>

</body>
</html>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
    function validaForm() {
        if ($("#requerimiento").val()=="") {
            alert("Debe de introducir un requerimiento.");
            $("#requerimiento").focus();
            return false;
        }
        if($("#fuente").val()=="") {
            alert("Debe de introducir la fuente (nombre de quien lo solicito).");
            $("#fuente").focus();
            return false;
        }

        return true;
    }
    $(document).ready(function(){
        $("#boton-guardar").click(function() {
            if(validaForm()) {
                $("#formulario").submit();
            } else{
            	fds
            }
        });
    });

</script>