CREATE SEQUENCE seq_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE SEQUENCE seq_proyecto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE SEQUENCE seq_requerimiento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE SEQUENCE seq_historia
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE SEQUENCE seq_caso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE SEQUENCE seq_actor
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE TABLE usuario(
	idUsuario NUMERIC(2,0) DEFAULT nextval('seq_usuario'),
	nombre VARCHAR(30) NOT NULL,
	apellidoPaterno VARCHAR(30) NOT NULL,
	apellidoMaterno VARCHAR(30),
	correo VARCHAR(30) NOT NULL,
	contrasenia VARCHAR(30) NOT NULL,
	CONSTRAINT pkUsuario
	PRIMARY KEY (idUsuario),
	CONSTRAINT uqUsuarioCorreo
	UNIQUE(correo)
);

CREATE TABLE proyecto(
	idProyecto NUMERIC(3,0) DEFAULT nextval('seq_proyecto'),
	nombre VARCHAR(30) NOT NULL,
	descripcion TEXT NOT NULL,
	objetivo TEXT NOT NULL,
	fechaInicio DATE NOT NULL,
	fechaFin DATE NOT NULL,
	CONSTRAINT pkProyecto
	PRIMARY KEY(idProyecto),
	CONSTRAINT uqProyectoNombre
	UNIQUE(nombre)
);

CREATE TABLE requerimiento(
	idReq NUMERIC(4,0) DEFAULT nextval('seq_requerimiento'),
	idProyecto NUMERIC(3,0) NOT NULL,
	req TEXT NOT NULL,
	descripcion VARCHAR(100),
	fuente VARCHAR(25),
	fechaCreacion DATE NOT NULL,
	idOriginal NUMERIC(4,0),
	creador NUMERIC(2,0) NOT NULL,
	CONSTRAINT pkReq
	PRIMARY KEY(idReq),
	CONSTRAINT fkReq_Proyecto
	FOREIGN KEY(idProyecto)
	REFERENCES proyecto(idProyecto)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);

CREATE TABLE historia(
	idHistoria NUMERIC(4,0) DEFAULT nextval('seq_historia'),
	nombre VARCHAR(40) NOT NULL,
	historia TEXT NOT NULL,
	criterios TEXT NOT NULL,
	fechaCreacion DATE NOT NULL,	
	CONSTRAINT pkHistoria
	PRIMARY KEY(idHistoria)
);

CREATE TABLE caso(
	idCaso NUMERIC(4,0) DEFAULT nextval('seq_caso'),
	titulo VARCHAR(30) NOT NULL,
	descripcion TEXT NOT NULL,
	precondiciones TEXT NOT NULL,
	flujoPrincipal TEXT NOT NULL,
	flujosAlternos TEXT,
	flujosExcepcion TEXT,
	poscondiciones TEXT NOT NULL,
	notas TEXT NOT NULL,
	CONSTRAINT pkCaso
	PRIMARY KEY(idCaso)	
);

CREATE TABLE actor(
	idActor NUMERIC(4,0) DEFAULT nextval('seq_actor'),
	nombre VARCHAR(25) NOT NULL,
	descripcion VARCHAR(100) NOT NULL,
	CONSTRAINT pkActor
	PRIMARY KEY(idActor)
);

CREATE TABLE usuario_proyecto(
	idUsuario NUMERIC(2,0),
	idProyecto NUMERIC(3,0),
	duenio NUMERIC(2,0) NOT NULL,
	CONSTRAINT pkUsuario_Proyecto
	PRIMARY KEY (idUsuario, idProyecto),
	CONSTRAINT fkUsuario_ProyectoUsuario
	FOREIGN KEY (idUsuario)
	REFERENCES usuario(idUsuario)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT,
	CONSTRAINT fkUsuario_ProyectoProyecto
	FOREIGN KEY (idProyecto)
	REFERENCES proyecto(idProyecto)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
);	

CREATE TABLE requerimiento_historia(
	idReq NUMERIC(4,0),
	idHistoria NUMERIC(4,0),
	CONSTRAINT pkReq_Historia
	PRIMARY KEY (idReq, idHistoria),
	CONSTRAINT fkReq_HistoriaReq
	FOREIGN KEY (idReq)
	REFERENCES requerimiento(idReq)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT,
	CONSTRAINT fkReq_HistoriaHistoria
	FOREIGN KEY (idHistoria)
	REFERENCES historia(idHistoria)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
);

CREATE TABLE requerimiento_caso(
	idReq NUMERIC(4,0),
	idCaso NUMERIC(4,0),
	CONSTRAINT pkReq_Caso
	PRIMARY KEY (idReq, idCaso),
	CONSTRAINT fkReq_CasoReq
	FOREIGN KEY (idReq)
	REFERENCES requerimiento(idReq)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT,
	CONSTRAINT fkReq_CasoCaso
	FOREIGN KEY (idCaso)
	REFERENCES caso(idCaso)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
);	

CREATE TABLE caso_actor(
	idCaso NUMERIC(4,0),
	idActor NUMERIC(4,0),
	CONSTRAINT pkCaso_Actor
	PRIMARY KEY (idCaso, idActor),
	CONSTRAINT fkCaso_ActorCaso
	FOREIGN KEY (idCaso)
	REFERENCES caso(idCaso)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT,
	CONSTRAINT fkCaso_ActorActor
	FOREIGN KEY (idActor)
	REFERENCES actor(idActor)
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
);		

